/**
 * Annoyed by manually having to press the Run/Test buttons in Code Expert (https://expert.ethz.ch/)?
 * Fear no more, just paste the following lines into the JavaScript console of your browser on the
 * Code Expert page and it will allow you to run the code, simply by pressing `CTRL+S`.
 * Adjust it yourself if you want a different key combination to run things in Code Expert.
 */

// Register function for keypress
window.onkeyup = function (e) {
    // If `CTRL+S`
    if (e.ctrlKey && e.keyCode == 83) {
        // Press the flask button
        document.querySelector('[title="Test"]').click();
        // Replace "Test" with "Run" if you want to run the your code on `CTRL+S`
    }
}
